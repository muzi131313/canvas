var Index = {
    init() {
        let canvas = document.querySelector('#canvas');
        let ctx = canvas.getContext('2d');
        let {width, height} = canvas;
        this.drawLine(ctx, width, height);
    },
    drawLine(ctx, width, height) {
        ctx.strokeStyle = 'hsl(0, 80%, 60%)';
        ctx.lineWidth = 5;
        ctx.beginPath();
        ctx.moveTo(0, height / 2);
        ctx.lineTo(width, height / 2);
        ctx.stroke();
        ctx.closePath();
    }
}

window.onload = function (params) {
    Index.init();
}
