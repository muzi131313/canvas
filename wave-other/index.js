'use strict';

setTimeout(function() {
    var canvasEl = document.getElementById('js-canvas');
    var width = canvasEl.width;
    var height = canvasEl.height;

    var canvasCtx = canvasEl.getContext('2d');
    var fundamental = 300;
    var amplitude = height / 2 - 10;
    var waveCount = 3;
    var colorStepSize = 360 / (waveCount + 1);
    var waveforms = Array.apply(undefined, Array(waveCount)).map(function(_, index) {
        var n = (index + 1) * 2 - 1;
        var color = 'hsl(' + Math.floor(colorStepSize * (index + 1)) + ', 50%, 30%)';
        return waveformFactory.create(color, amplitude / n, fundamental / n);
    });
    console.log('waveforms: ', waveforms);
    var waveformVisualizer = waveformVisualizerFactory.create(fundamental);
    animate(0, canvasCtx, width, height, waveformVisualizer, waveforms);
}, 0);

function animate(iteration, ctx, width, height, waveformVisualizer, waveforms) {
    ctx.clearRect(0, 0, width, height);
    waveformVisualizer.render(ctx, iteration, waveforms);
    requestAnimationFrame(function() {
        animate(iteration + 1, ctx, width, height, waveformVisualizer, waveforms);
    }, 1000 / 60);
}

var waveformFactory = {
    create: function create(color, amplitude, wavelength) {
        var offsetTheta = arguments.length <= 3 || arguments[3] === undefined ? 0 : arguments[3];

        var state = {
            color: color,
            amplitude: amplitude,
            wavelength: wavelength,
            offsetTheta: offsetTheta
        };
        return state;
    }
};

var waveformVisualizerFactory = {
    create: function create(stepCount) {
        var state = {
            stepCount: stepCount,
            render: function render(ctx, iteration, waveforms) {
                return waveformVisualizerFactory.render(state, ctx, iteration, waveforms);
            }
        };
        return state;
    },

    drawMidline: function drawMidline(ctx, width, height) {
        ctx.strokeStyle = 'hsl(0, 80%, 60%)';
        ctx.lineWidth = 5;
        ctx.beginPath();
        ctx.moveTo(0, height / 2);
        ctx.lineTo(width, height / 2);
        ctx.stroke();
        ctx.closePath();
    },

    drawWave: function drawWave(ctx, iteration, width, height, stepCount, _ref) {
        var amplitude = _ref.amplitude;
        var wavelength = _ref.wavelength;
        var color = _ref.color;
        var offsetTheta = _ref.offsetTheta;

        var stepSize = width / stepCount;
        ctx.lineJoin = 'round';
        ctx.beginPath();
        ctx.strokeStyle = color;
        var hasMoved = false;
        Array.apply(undefined, Array(stepCount + 1)).forEach(function(_, index) {
            var theta = ((1 - (index + iteration) / wavelength % 1) * Math.PI * 2 + offsetTheta) % (Math.PI * 2);
            var y = amplitude * Math.sin(theta);
            if (!hasMoved) {
                ctx.moveTo(stepSize * index, y + height / 2);
                hasMoved = true;
            } else {
                ctx.lineTo(stepSize * index, y + height / 2);
            }
        });
        ctx.stroke();
        ctx.closePath();
    },

    drawSignal: function drawSignal(ctx, iteration, width, height, stepCount, waveforms) {
        var stepSize = width / stepCount;
        ctx.lineJoin = 'round';
        ctx.beginPath();
        ctx.strokeStyle = '#fff';
        ctx.moveTo(0, height / 2);
        var hasMoved = false;
        Array.apply(undefined, Array(stepCount + 1)).forEach(function(_, index) {
            var y = 0;
            waveforms.forEach(function(_ref2, waveformIndex) {
                var wavelength = _ref2.wavelength;
                var amplitude = _ref2.amplitude;
                var offsetTheta = _ref2.offsetTheta;

                var theta = ((1 - (index + iteration) / wavelength % 1) * Math.PI * 2 + offsetTheta) % (Math.PI * 2);
                y += amplitude * Math.sin(theta);
            });
            if (!hasMoved) {
                ctx.moveTo(stepSize * index, y + height / 2);
                hasMoved = true;
            } else {
                ctx.lineTo(stepSize * index, y + height / 2);
            }
        });
        ctx.stroke();
        ctx.closePath();
    },

    render: function render(state, ctx, iteration, waveforms) {
        var _ctx$canvas = ctx.canvas;
        var width = _ctx$canvas.width;
        var height = _ctx$canvas.height;

        // waveformVisualizerFactory.drawMidline(ctx, width, height);
        waveforms.forEach(function(waveform) {
            waveformVisualizerFactory.drawWave(ctx, iteration, width, height, state.stepCount, waveform);
        });
        // waveformVisualizerFactory.drawSignal(ctx, iteration, width, height, state.stepCount, waveforms);
    }
};
